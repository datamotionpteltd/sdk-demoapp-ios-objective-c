# Raxel Demo App
This is an app that demostrate using of [RaxelPulse framework](https://developer.raxeltelematics.com). The app will track the person's driving behavior such as speeding, turning, braking and several other things. 

## Installation (Mac)

To run the demo app: 

 * clone this repository to local folder 
 * run `pod install` from project's folder.
 * open `AppDelegate.swift` and insert your Device Token to this: 
 
```
[RPEntry instance].virtualDeviceToken = @"<some virtual device token>";
```

 * implement code signing if you want to install this app to your iOS device

