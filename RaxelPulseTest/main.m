//
//  main.m
//  RaxelPulseTest
//
//  Created by Igor Nabokov on 12/12/2018.
//  Copyright © 2018 Raxel Telematics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
