//
//  AppDelegate.h
//  RaxelPulseTest
//
//  Created by Igor Nabokov on 12/12/2018.
//  Copyright © 2018 Raxel Telematics. All rights reserved.
//

#import <UIKit/UIKit.h>
@import UserNotifications;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

