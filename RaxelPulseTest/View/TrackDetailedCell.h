//
//  TrackDetailedCell.h
//  K
//
//  Created by Георгий Малюков on 19.09.16.
//  Copyright © 2016 Raxel Telematics. All rights reserved.
//



#import <UIKit/UIKit.h>
#import <RaxelPulse/RaxelPulse.h>

@interface TrackDetailedCell : UITableViewCell

- (void)updateWithTrack:(RPTrackProcessed *)track;

@end
