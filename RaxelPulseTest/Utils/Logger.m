//
//  Logger.m
//  RaxelPulseTest
//
//  Created by Igor Nabokov on 12/12/2018.
//  Copyright © 2018 Raxel Telematics. All rights reserved.
//

#import "Logger.h"

@implementation Logger
+ (instancetype)sharedInstance {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });

    return _sharedInstance;
}

- (void)logEvent:(NSString *)event {
    NSLog(@"event: %@", event);
}

- (void)logWarning:(NSString *)warning {
    NSLog(@"warning: %@", warning);
}

@end
