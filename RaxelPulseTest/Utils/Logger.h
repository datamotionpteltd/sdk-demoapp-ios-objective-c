//
//  Logger.h
//  RaxelPulseTest
//
//  Created by Igor Nabokov on 12/12/2018.
//  Copyright © 2018 Raxel Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RaxelPulse/RaxelPulse.h>

NS_ASSUME_NONNULL_BEGIN

@interface Logger : NSObject <RPLogDelegate>

- (void)logEvent:(NSString *)event;
- (void)logWarning:(NSString *)warning;

+ (instancetype)sharedInstance;

@end

NS_ASSUME_NONNULL_END
