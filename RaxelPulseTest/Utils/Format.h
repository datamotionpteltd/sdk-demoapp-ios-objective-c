//
//  Format.h
//  RaxelPulseTest
//
//  Created by Igor Nabokov on 13/12/2018.
//  Copyright © 2018 Raxel Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Format : NSObject

+ (NSCalendar *)calendar;
+ (NSDateFormatter *)dateFormatterDayMonth;
+ (NSDateFormatter *)dateFormatterTimeHoursMinutes;
+ (NSDateFormatter *)dateFormatterDurationHoursMinutesShort;

@end