//
//  TrackOrigin.h
//  RaxelPulseTest
//
//  Created by Igor Nabokov on 13/12/2018.
//  Copyright © 2018 Raxel Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TrackOriginType) {
    TrackOriginTypeUnknown,
    TrackOriginTypeOriginalDriver,
    TrackOriginTypeCar,
    TrackOriginTypeBus,
    TrackOriginTypeTaxi,
    TrackOriginTypeTrain,
    TrackOriginTypeBicycle,
    TrackOriginTypeMotorcycle,
    TrackOriginTypeWalking,
    TrackOriginTypeRunning,
    TrackOriginTypeOther
};


@interface TrackOrigin : NSObject

+ (UIImage *)imageForType:(TrackOriginType)state wasChangedByUser:(BOOL)changed;
+ (TrackOriginType)typeFromTrackOriginCode:(NSString *)code;

@end
