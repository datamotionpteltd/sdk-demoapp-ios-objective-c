//
//  TracksViewController.h
//  RaxelPulseTest
//
//  Created by Igor Nabokov on 13/12/2018.
//  Copyright © 2018 Raxel Telematics. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TracksViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end

NS_ASSUME_NONNULL_END
